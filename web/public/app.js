var CODE_URL = '';

async function program_main() {
    //Wait for code url
    const codeData = await MPI_Recv_Broadcast();
    const devicesCount = codeData.count;

    CODE_URL = codeData.url;

    log(`Code URL is: ${CODE_URL}`);
    log(`No. of Device to use: ${devicesCount}`);

    //wait for all other proccesses to confirm
    if (EDGE_RANK == 0) {
        for (var i = 1; i < devicesCount; i++) {
            const result = await MPI_Recv(i);
            log(`Node ${i} is ready...`);
        }

        //import remote file
        const { foo } = await import(CODE_URL);
    }
    else {
        //import remote file
        const { foo } = await import(CODE_URL);

        //Tell master process they are ready
        MPI_Send(0, true);
    }    
}

program_main();