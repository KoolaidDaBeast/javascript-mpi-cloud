const SIZE = 1000;
var A = [];
var B = [];
var C = [];

// Javascrip Functions //
Array.prototype.extend = function (other_array) {
    other_array.forEach(function(v) {this.push(v)}, this);
}

function generateMatrix() {
    for (var i = 0; i < SIZE; i++) {
        for (var z = 0; z < SIZE; z++) {
            A[i * SIZE + z] = Math.round(Math.random() * 10);
        }
    }

    for (var i = 0; i < SIZE; i++) {
        for (var z = 0; z < SIZE; z++) {
            B[i * SIZE + z] = Math.round(Math.random() * 10);
        }
    }
}

function matrixToString(title, mat) {
    var strresult = `------ ${title} ------ \n`;

    const rowsCount = mat.length / SIZE;

    for (var i = 0; i < rowsCount; i++) {
        for (var z = 0; z < SIZE; z++) {
            if (z == (SIZE - 1)) { strresult += mat[i * SIZE + z] }
            else { strresult += mat[i * SIZE + z] + ', '; }
        }

        strresult += '\n';
    }

    return strresult;
}

function matrixToStringFull(title, mat) {
    var strresult = `------ ${title} ------ \n`;

    const rowsCount = SIZE;

    for (var i = 0; i < rowsCount; i++) {
        for (var z = 0; z < SIZE; z++) {
            if (z == (SIZE - 1)) { strresult += mat[i * SIZE + z] }
            else { strresult += mat[i * SIZE + z] + ', '; }
        }

        strresult += '\n';
    }

    return strresult;
}

function calculate(matrixA, matrixB) {
    const rowsCount = matrixA.length / SIZE;

    for (var i = 0; i < rowsCount; i++) {
        for (var z = 0; z < SIZE; z++) {
            var res = 0;

            for (var iA = 0; iA < SIZE; iA++) {
                res += matrixA[i * SIZE + iA] * matrixB[iA * SIZE + z];
            }
            
            C[i * SIZE + z] = res;
        }
    }
}

async function head(rank, edgeCount) {
    //Generate matrix
    generateMatrix();
    log('Generated matrices A and B of size ' + SIZE);
    //log(matrixToString('---- Full Matrix A -----', A));
    //log(matrixToString('---- Full Matrix B -----', B));

    const chunkSize = (SIZE * SIZE) / edgeCount;
    MPI_Send_Broadcast(B);
    log('Sent Matrix B to all processes');

    const timeStart = new Date();

    //send chunks to processes
    for (var i = 0; i < edgeCount; i++) {
        if (i == 0) continue;

        const chunkStart = i * chunkSize;
        var chunkEnd = chunkStart + chunkSize;
        if (chunkEnd >= A.length) { chunkEnd = A.length - 1; }
        const chunk = A.slice(chunkStart, chunkEnd);

        MPI_Send(i, chunk);
        log('Sent Sub Matrix A to process ' + i);
    }

    //calculate masters chunk
    const subA = A.slice(0, chunkSize);
    calculate(subA, B);

    //wait for chunks to processes
    for (var i = 0; i < edgeCount; i++) {
        if (i == 0) continue;
        
        const chunk = await MPI_Recv(i);
        chunk.forEach(element => { C.push(element); });
        //log(matrixToString('Chunk from process ' + i, chunk));
        log('Chunk from process ' + i + ' is done.');
    }

    const timeEnd = new Date();

    log(`Time taken: ${(timeEnd - timeStart) / 1000}`);

    MPI_Download('results.txt', matrixToStringFull('Matrix C', C));

    log('Done...');
}

async function node(rank, edgeCount) {
    B = await MPI_Recv_Broadcast();
    //printMatrix('Recieved Matrix B', B);
    log('Recieved Matrix B');
    //log(matrixToString('---- Matrix B -----', B));

    A = await MPI_Recv(0);
    //printMatrix('Recieved Matrix A', A);
    log('Recieved Matrix A');
    //log(matrixToString('---- Sub of A -----', A));

    calculate(A, B);
    log('Done...');

    MPI_Send(0, C);
    log('Sent back to process 0');
}

async function main() {
    const rank = MPI_Edge_Rank();
    const edgeCount = parseInt("{EDGE_COUNT}");

    if (rank == 0) {
        await head(rank, edgeCount);
    }
    else {
        await node(rank, edgeCount);
    }
}

main();