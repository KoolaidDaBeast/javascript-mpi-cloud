var app = require('express')();
const cors = require('cors');
const fileupload = require('express-fileupload');

app.use(cors());
app.use(fileupload());

const server = require('http').Server(app);
const io = require('socket.io')(server);
var middleware = require('socketio-wildcard')();

const port = process.env.PORT || 4000;
io.use(middleware);

console.log("Server will be listening on port " + port);
server.listen(port);

function emit(io, namespace, room, data) {
    io.of('/' + namespace).emit(room, JSON.stringify(data));
}

app.get('/*', function (req, res) {
    return res.send('This socket test is working!');
});

//MPI Variables
var DevicesCount = 0;

io.on("connection", function (socket) {
    //Device has connected
    DevicesCount++;
    console.log(`[SERVER] A Device has connected there is now ${DevicesCount} devices.`);

    //Device has disconnected
    socket.on('disconnect', function (packet) {
        DevicesCount--;
        console.log(`[SERVER] A Device has disconnected there is now ${DevicesCount} devices.`);
    });

    //Get Number of devices
    socket.on('Device_Count', function (packet) {
        socket.emit('Device_Count', DevicesCount);
    });

    //Forward all Process events to all  nodes
    socket.on('*', function (packet) {
        const event = packet.data[0];
        const eventData = packet.data[1];

        //if (!event.toLowerCase().startsWith("Process_")) { return; }

        io.emit(event, eventData);
        console.log(event + "=" + eventData);
    });
});