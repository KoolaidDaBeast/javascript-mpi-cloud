var covidData = [];
const devicesCount = parseInt("{EDGE_COUNT}");

async function head() {
    log(`${covidData.length} covid results downloaded.`);

    //Calculate chunk sizes to send
    const chunkSize = covidData.length / devicesCount;

    //Send chunks to processes
    for (var i = 0; i < devicesCount; i++) {
        if (i == 0) continue;

        const chunkStart = i * chunkSize;
        var chunkEnd = (chunkStart + chunkSize) >= covidData.length ? covidData.length - 1 : (chunkStart + chunkSize);
        const chunk = covidData.slice(chunkStart, chunkEnd);

        MPI_Send(i, chunk);
    }
}

async function node() {
    covidData = await MPI_Recv(0);

    log(`${covidData.length} covid results have been recieved.`);

    console.log(covidData[0]);

    var covidResults = {};
}

async function main() {
    const rank = MPI_Edge_Rank();

    if (rank == 0) {
        log("Downloading US covid data this will take several minutes.");

        $.get('https://javascript-mpi-cloud-api-6c4e6edf2c57.herokuapp.com/united_states.json', (data) => {
            covidData = data;
        });

        var itId = setInterval(async () => {
            if (covidData.length > 0) {
                await head();
                clearInterval(itId);
            }
        }, 10);
    }
    else {
        await node();
    }

}

main();