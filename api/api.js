const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs').promises;
const app = express();

const port = process.env.PORT || 2000;
const base = `${__dirname}/public`;

//Setting Cross-Origin Headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, REQUEST");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");

    next();
});

//Setting express to use body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(cookieParser());

app.use(express.static('users'));

app.post('/add-script', async (req, res) => {
    const {filename, text} = req.body;

    console.log("Uploading file " + filename + "...");

    //Create public folder
    await fs.mkdir('./public', { recursive: true });

    await fs.writeFile(`./public/${filename}`, text);

    return res.json({
        success: true
    });
});

app.get('/:filename', (req, res) => {
    const { filename } = req.params;
    res.sendFile(`${base}/${filename}`);
});

//Web Listener\\
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});